 
__author__ = 'Julian Stirling'

from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))

setup(name = 'SciQtWidgets',
      version = '0.0.1',
      install_requires=['pyqt5==5.9', 'scipy'],
      description = 'SciQtWidgets is a collections of pyQt5 widgets for use in scientific software.',
      long_description = 'SciQtWidgets is a collections of pyQt5 widgets for use in scientific software.',
      author = 'Julian Stirling',
      author_email = 'julian@julianstirling.co.uk',
      packages=['SciQtWidgets'],
      keywords = ['scientific','Widget'],
      zip_safe = True,
      classifiers = [
          'Development Status :: 4 - Beta',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 2.7'
          ],
      )

