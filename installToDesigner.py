#!/usr/bin/env python
try:
    from PyQt5.QtCore import QLibraryInfo
except ImportError:
    print("Cannot install SciQtWidgets without PyQt5")

import os

here = os.path.abspath(os.path.dirname(__file__))
os.chdir(here)
qtpluginpath = QLibraryInfo.location(QLibraryInfo.PluginsPath)
pythonpluginpath = os.path.join(str(qtpluginpath), "designer", "python")
if not os.path.exists(pythonpluginpath):
    os.makedirs(pythonpluginpath)

pluginfile = "SciQtWidgetsplugin.py"
pluginpath = os.path.join(here, "SciQtWidgets", pluginfile)
print("Copying %s to %s"%(pluginpath, pythonpluginpath))
open(os.path.join(pythonpluginpath, pluginfile), "wb").write(open(pluginpath, "rb").read())

iconfolder = "SciQtWidgetIcons"
icondir = os.path.join(here, "SciQtWidgets", iconfolder) 
icontarget = os.path.join(pythonpluginpath, iconfolder)
if not os.path.exists(icontarget):
    os.makedirs(icontarget)

for icon in os.listdir(icondir):
    print("Copying %s to %s"%(icon,icontarget))
    open(os.path.join(icontarget, icon), "wb").write(open(os.path.join(icondir, icon), "rb").read())